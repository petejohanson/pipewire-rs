extern crate pipewire_sys as ffi;

pub struct Type {
    raw: *mut ffi::pw_type,
}

impl Type {
    pub fn from_raw(raw: *mut ffi::pw_type) -> Self {
        Type { raw }
    }

    // TODO: Accessors for details of the raw type struct!
}
