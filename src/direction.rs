extern crate pipewire_sys as ffi;

pub enum Direction {
    Input,
    Output,
}

pub fn direction_to_ffi_direction(dir: Direction) -> ffi::pw_direction {
    match dir {
        Direction::Input => ffi::pw_direction::PW_DIRECTION_INPUT,
        Direction::Output => ffi::pw_direction::PW_DIRECTION_OUTPUT,
    }
}
