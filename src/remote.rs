extern crate pipewire_sys as ffi;

pub struct Remote {
    raw: *mut ffi::pw_remote,
}

impl Remote {
    pub fn new(core: &crate::Core, props: Option<&crate::Properties>) -> Self {
        unsafe {
            Remote {
                raw: ffi::pw_remote_new(
                    core.as_mut_ptr(),
                    props
                        .map(|p| p.as_mut_ptr())
                        .unwrap_or(std::ptr::null_mut()),
                    0,
                ), // What is user_data_size?
            }
        }
    }

    pub fn as_mut_ptr(&self) -> *mut ffi::pw_remote {
        self.raw
    }

    pub unsafe fn connect(&mut self) -> Result<(), &str> {
        if ffi::pw_remote_connect(self.raw) < 0 {
            Err("Unable to connect the remote")
        } else {
            Ok(())
        }
    }
}

impl Drop for Remote {
    fn drop(&mut self) -> () {
        unsafe { ffi::pw_remote_destroy(self.raw) }
    }
}
