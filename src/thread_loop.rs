use std::ffi::CString;

extern crate pipewire_sys as ffi;

use crate::utils::err_code_to_result;

pub struct ThreadLoop {
    raw: *mut ffi::pw_thread_loop,
}

impl ThreadLoop {
    pub fn new(l: &crate::Loop, name: &str) -> Self {
        unsafe {
            ThreadLoop {
                raw: ffi::pw_thread_loop_new(l.as_mut_ptr(), CString::new(name).unwrap().as_ptr()),
            }
        }
    }

    pub fn lock(&mut self) -> ThreadLoopLock {
        unsafe { ffi::pw_thread_loop_lock(self.raw) }

        ThreadLoopLock { r#loop: self }
    }

    pub unsafe fn start(&mut self) -> Result<(), &str> {
        err_code_to_result(
            ffi::pw_thread_loop_start(self.raw),
            "Error starting main loop",
        )
    }

    // pub fn add_listener(&mut self) -> () {
    // unsafe { ffi::pw_thread_loop_add_listener(self.raw) }
    // }

    pub fn stop(&mut self) -> () {
        unsafe { ffi::pw_thread_loop_stop(self.raw) }
    }

    pub fn wait(&mut self) -> () {
        unsafe { ffi::pw_thread_loop_wait(self.raw) }
    }

    pub fn accept(&mut self) -> () {
        unsafe { ffi::pw_thread_loop_accept(self.raw) }
    }

    pub fn signal(&mut self, wait_for_accept: bool) -> () {
        unsafe { ffi::pw_thread_loop_signal(self.raw, wait_for_accept) }
    }

    pub fn in_thread(&mut self) -> bool {
        unsafe { ffi::pw_thread_loop_in_thread(self.raw) }
    }
}

impl Drop for ThreadLoop {
    fn drop(&mut self) -> () {
        unsafe { ffi::pw_thread_loop_destroy(self.raw) }
    }
}

pub struct ThreadLoopLock<'a> {
    r#loop: &'a ThreadLoop,
}

impl Drop for ThreadLoopLock<'_> {
    fn drop(&mut self) -> () {
        unsafe { ffi::pw_thread_loop_unlock(self.r#loop.raw) }
    }
}
