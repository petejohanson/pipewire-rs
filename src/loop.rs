extern crate pipewire_sys as ffi;

pub struct Loop {
    raw: *mut ffi::pw_loop,
}

impl Loop {
    pub unsafe fn new(props: Option<&crate::Properties>) -> Self {
        Loop {
            raw: ffi::pw_loop_new(
                props
                    .map(|p| p.as_mut_ptr())
                    .unwrap_or(std::ptr::null_mut()),
            ),
        }
    }

    pub fn from_raw(raw: *mut ffi::pw_loop) -> Self {
        Loop { raw }
    }

    pub fn as_mut_ptr(&self) -> *mut ffi::pw_loop {
        self.raw
    }
}

impl Drop for Loop {
    fn drop(&mut self) -> () {
        unsafe { ffi::pw_loop_destroy(self.raw) }
    }
}
