use std::ffi::{CStr, CString};

extern crate pipewire_sys as ffi;

use crate::utils::err_code_to_result;

pub struct Stream {
    raw: *mut ffi::pw_stream,
}

bitflags! {
    pub struct Flags: u32 {
        const NONE = ffi::pw_stream_flags::PW_STREAM_FLAG_NONE.0;
        const AUTOCONNECT = ffi::pw_stream_flags::PW_STREAM_FLAG_AUTOCONNECT.0;
        const INACTIVE = ffi::pw_stream_flags::PW_STREAM_FLAG_INACTIVE.0;
        const MAP_BUFFERS = ffi::pw_stream_flags::PW_STREAM_FLAG_MAP_BUFFERS.0;
        const DRIVER = ffi::pw_stream_flags::PW_STREAM_FLAG_DRIVER.0;
        const RT_PROCESS = ffi::pw_stream_flags::PW_STREAM_FLAG_RT_PROCESS.0;
        const NO_CONVERT = ffi::pw_stream_flags::PW_STREAM_FLAG_NO_CONVERT.0;
        const EXCLUSIVE = ffi::pw_stream_flags::PW_STREAM_FLAG_EXCLUSIVE.0;
    }
}

bitflags! {
    pub struct State: i32 {
        const ERROR = ffi::pw_stream_state::PW_STREAM_STATE_ERROR.0;
    }
}

#[derive(Default)]
pub struct StreamEvents {
    pub state_changed: Option<Box<FnMut(State, State, Option<&str>)>>,
    pub format_changed: Option<Box<FnMut(ffi::spa_pod)>>,
}

unsafe extern "C" fn state_changed(
    data: *mut ::std::os::raw::c_void,
    old: ffi::pw_stream_state,
    state: ffi::pw_stream_state,
    error: *const ::std::os::raw::c_char,
) {
    dbg!("state_changed: ENTER");
    let mut events = Box::from_raw(data as *mut StreamEvents);

    dbg!("state_changed: Got events");
    if let Some(ref mut sc) = events.state_changed {
        dbg!("Got state changed");
        sc(
            State::from_bits(old.0).unwrap(),
            State::from_bits(state.0).unwrap(),
            CStr::from_ptr(error).to_str().ok(),
        );
    }

    Box::into_raw(events);
}

unsafe extern "C" fn format_changed(
    data: *mut ::std::os::raw::c_void,
    format: *const ffi::spa_pod,
) {
    let mut events = Box::from_raw(data as *mut StreamEvents);

    if let Some(ref mut fc) = events.format_changed {
        dbg!("Got state changed");
        fc(*format);
    }

    Box::into_raw(events);
}

impl Stream {
    pub fn new(remote: &crate::Remote, name: &str, props: Option<&crate::Properties>) -> Self {
        unsafe {
            Stream {
                raw: ffi::pw_stream_new(
                    remote.as_mut_ptr(),
                    CString::new(name).unwrap().as_ptr(),
                    props
                        .map(|p| p.as_mut_ptr())
                        .unwrap_or(std::ptr::null_mut()),
                ),
            }
        }
    }

    pub unsafe fn add_listener(&mut self, events: StreamEvents) -> impl FnOnce() {
        // Pass in listener pointer!
        let ffi_events = ffi::pw_stream_events {
            version: 0, // TODO: ffi::PW_VERSION_STREAM_EVENTS,
            state_changed: Some(state_changed),
            format_changed: Some(format_changed),
            process: None,
            add_buffer: None,
            remove_buffer: None,
            destroy: None,
        };

        let mut hook: ffi::spa_hook = ffi::spa_hook {
            link: ffi::spa_list {
                next: std::ptr::null_mut(),
                prev: std::ptr::null_mut(),
            },
            funcs: std::ptr::null(),
            data: std::ptr::null_mut(),
            priv_: std::ptr::null_mut(),
            removed: None,
        };

        ffi::pw_stream_add_listener(
            self.raw,
            &mut hook,
            &ffi_events,
            Box::into_raw(Box::new(events)) as *mut std::ffi::c_void,
        );

        move || drop(hook) // TODO: This can't be right. Must be something better.
    }

    // TODO: Params!
    pub unsafe fn connect(
        &mut self,
        direction: crate::Direction,
        port_path: &str,
        flags: Flags,
    ) -> Result<(), &str> {
        let err_code = ffi::pw_stream_connect(
            self.raw,
            crate::direction::direction_to_ffi_direction(direction),
            CString::new(port_path).unwrap().as_ptr(),
            ffi::pw_stream_flags(flags.bits),
            std::ptr::null_mut(),
            0,
        );

        err_code_to_result(err_code, "Failed to connect the stream")
    }

    pub unsafe fn disconnect(&mut self) -> Result<(), &str> {
        err_code_to_result(ffi::pw_stream_disconnect(self.raw), "Unale to disconnect")
    }

    pub unsafe fn set_active(&mut self, active: bool) -> Result<(), &str> {
        err_code_to_result(
            ffi::pw_stream_set_active(self.raw, active),
            "Failed to set stream active state",
        )
    }

    pub unsafe fn get_state(&self) -> Result<ffi::pw_stream_state, &str> {
        let e: *mut (*const std::os::raw::c_char) = std::ptr::null_mut();
        let state = ffi::pw_stream_get_state(self.raw, e);

        if e != std::ptr::null_mut() {
            Err(CStr::from_ptr(*e).to_str().unwrap())
        } else {
            Ok(state)
        }
    }
}

impl Drop for Stream {
    fn drop(&mut self) -> () {
        unsafe { ffi::pw_stream_destroy(self.raw) }
    }
}
