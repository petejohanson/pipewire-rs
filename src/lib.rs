#[macro_use]
extern crate bitflags;

mod utils;

pub mod properties;
pub use properties::Properties;

mod direction;
pub use direction::Direction;

pub mod r#loop;
pub use r#loop::Loop;

pub mod thread_loop;
pub use thread_loop::{ThreadLoop, ThreadLoopLock};

pub mod r#type;
pub use r#type::Type;

pub mod core;
pub use crate::core::Core;

pub mod remote;
pub use remote::Remote;

pub mod stream;
pub use stream::{Flags, Stream, StreamEvents};

// TODO: ARGC/ARGV?
pub fn init() {
    unsafe { pipewire_sys::pw_init(std::ptr::null_mut(), std::ptr::null_mut()) }
}
