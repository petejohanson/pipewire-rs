extern crate pipewire_sys as ffi;

pub struct Core {
    raw: *mut ffi::pw_core,
}

impl Core {
    pub fn new(r#loop: &crate::Loop, props: Option<&crate::Properties>) -> Self {
        unsafe {
            Core {
                raw: ffi::pw_core_new(
                    r#loop.as_mut_ptr(),
                    props
                        .map(|p| p.as_mut_ptr())
                        .unwrap_or(std::ptr::null_mut()),
                ),
            }
        }
    }

    pub fn as_mut_ptr(&self) -> *mut ffi::pw_core {
        self.raw
    }

    pub unsafe fn get_main_loop(&self) -> crate::Loop {
        crate::Loop::from_raw(ffi::pw_core_get_main_loop(self.raw))
    }

    pub unsafe fn get_type(&self) -> crate::Type {
        crate::Type::from_raw(ffi::pw_core_get_type(self.raw))
    }
}

impl Drop for Core {
    fn drop(&mut self) -> () {
        unsafe { ffi::pw_core_destroy(self.raw) }
    }
}
