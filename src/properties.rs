extern crate pipewire_sys as ffi;

pub struct Properties {
    raw: *mut ffi::pw_properties,
}

impl Properties {
    pub fn as_mut_ptr(&self) -> *mut ffi::pw_properties {
        self.raw
    }
}
