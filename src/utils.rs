pub fn err_code_to_result(code: std::os::raw::c_int, error_string: &str) -> Result<(), &str> {
    if code < 0 {
        Err(error_string)
    } else {
        Ok(())
    }
}
